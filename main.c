#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <time.h>
#include <stdbool.h>

// Gotoxy
void gotoxy(int x,int y)
{
    COORD coord= {0,0};
    coord.X=x;
    coord.Y=y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),coord);
}

int main()
{
    char textura = 254;
    int limite,cuerpo,entrada;
    int i,j;
    COORD cabeza, cola;

    //Inicializar campo
    srand(time(NULL));
    limite = 30;
    bool campo[limite][limite];     //matriz. true=ocupado, false=libre

    for(i=0;i<limite;i++){
        for(j=0;j<limite;j++){
            campo[i][j]=false;
        }
    }
// ------------------ LABERINTO ------------------
    //Techo
    for(i=0;i<limite;i++){
        printf("%c",220);
        campo[i][0]=true;
    }
    printf("\n");

    //Paredes
    for(i=1;i<limite;i++){
         printf("%c",219);
         campo[0][i]=true;
         gotoxy(limite-1,i);
         printf("%c\n",219);
         campo[limite-1][i]=true;
    }

    //Piso
    for(i=0;i<limite;i++){
        printf("%c",223);
        campo[i][limite-1]=true;
    }
     // ------------------ serpiente ------------------
    cuerpo = 5;                 //Longitud inicial
    cola.X = rand() % (limite-cuerpo+1);
    cola.Y = rand() % limite+1;
    cabeza.X=cola.X+cuerpo-1;
    cabeza.Y=cola.Y;

    //Llenar serpiente
    gotoxy(cola.X,cola.Y);
    for(j=0;j<cuerpo;j++){
            printf("%c",textura);
            campo[cola.X+j][cola.Y] = true;
        }
// Mover serpiente
    for(int i=0;i<70;i++){
        entrada = getch();
        switch (entrada){
            case 119: //w
                cabeza.Y--;
                break;
            case 97: //a
                cabeza.X--;
                break;
            case 115: //s
                cabeza.Y++;
                break;
            case 100: //d
                cabeza.X++;
                break;
        }
        if(campo[cabeza.X][cabeza.Y]){
            printf("Tu pierdes!");
            break;
        }

        campo[cola.X][cola.Y] = false;
        gotoxy(cola.X,cola.Y);
        printf(" ");

        campo[cabeza.X][cabeza.Y] = true;
        gotoxy(cabeza.X,cabeza.Y);
        printf("%c",textura);
    }
    return 0;
}
